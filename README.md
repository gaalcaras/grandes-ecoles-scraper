# Scraper pour grandes écoles

- 3 ENS
- Écoles Paris Tech
- Centrale
- Sciences Po
- Polytechnique

## Prérequis

Python 2.7

```
pip install scrapy regex unidecode
```

## How to

```
cd grandes-ecoles-scraper
scrapy crawl SpiderName --logfile "log/spidername.log"
```