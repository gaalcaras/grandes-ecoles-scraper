# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import Join, MapCompose, TakeFirst
from w3lib.html import remove_tags

class Alumni(scrapy.Item):
    alumniId = scrapy.Field()

    lastName = scrapy.Field(
            input_processor=MapCompose(remove_tags),
            output_processor=Join(),
            )

    firstName = scrapy.Field(
            input_processor=MapCompose(remove_tags),
            output_processor=Join(),
            )

    usageName = scrapy.Field(
            input_processor=MapCompose(remove_tags),
            output_processor=Join(),
            )

    year = scrapy.Field(
            input_processor=MapCompose(remove_tags),
            output_processor=Join(),
            )

    school = scrapy.Field(
            input_processor=MapCompose(remove_tags),
            output_processor=Join(),
            )

    field = scrapy.Field(
            input_processor=MapCompose(remove_tags),
            output_processor=Join(),
            )

    status = scrapy.Field(
            input_processor=MapCompose(remove_tags),
            output_processor=Join(),
            )

    pass
