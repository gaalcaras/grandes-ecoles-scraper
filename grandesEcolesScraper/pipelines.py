# -*- coding: utf-8 -*-

import regex
from unidecode import unidecode

from scrapy import signals
from scrapy.exporters import CsvItemExporter
from scrapy.exceptions import DropItem

class processItems(object):

    def process_item(self, item, spider):
        for key in item.keys():
            match = regex.search(('(lastName|'
                'firstName|'
                'usageName)'), key)

            if match:
                # Remove duplicate, beginning and ending spaces
                item[key] = ' '.join(item[key].split())
                item[key] = item[key].title()

        hashBase = item['lastName'] + item['year']

        if item['firstName'] != '':
            hashBase += item['firstName']

        # Get rid of accents and non alphanum caracters
        hashKey = unidecode(hashBase)
        hashKey = ''.join(e for e in hashKey if e.isalnum())
        item['alumniId'] = hash(hashKey.lower())

        return item

class dropDuplicates(object):

    def __init__(self):
        self.ids_seen = set()

    def process_item(self, item, spider):
        if item['alumniId'] in self.ids_seen:
            raise DropItem("Duplicate item found: %s" % item)
        else:
            self.ids_seen.add(item['alumniId'])
            return item

class csvExport(object):

    def __init__(self):
        self.files = {}

    @classmethod
    def from_crawler(cls, crawler):
        pipeline = cls()
        crawler.signals.connect(pipeline.spider_opened, signals.spider_opened)
        crawler.signals.connect(pipeline.spider_closed, signals.spider_closed)
        return pipeline

    def spider_opened(self, spider):
        file = open('csv/%s_alumni.csv' % spider.name, 'w+b')
        self.files[spider] = file
        self.exporter = CsvItemExporter(file)
        self.exporter.fields_to_export = ['alumniId',  'year', 'school', 'firstName', 'lastName', 'usageName', 'field', 'status']
        self.exporter.start_exporting()

    def spider_closed(self, spider):
        self.exporter.finish_exporting()
        file = self.files.pop(spider)
        file.close()

    def process_item(self, item, spider):
        self.exporter.export_item(item)
        return item

