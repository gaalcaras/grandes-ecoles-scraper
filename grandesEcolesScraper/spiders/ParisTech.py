#!/usr/bin/python
# - coding: utf-8 -*-

import scrapy
import regex
import string

from scrapy.loader import ItemLoader
from grandesEcolesScraper.items import Alumni 

class ParisTechAlumniSpider(scrapy.Spider):
    name = 'ParisTech'
    start_urls = ['http://www.paristech-alumni.org/gene/main.php?base=53063'] 

    # Name format "Firstname LASTNAME (USAGENAME)"
    nameRe = regex.compile(('(?P<firstName>(\p{Lu}[\p{Ll},\-,\']+\s){1,4})'
            '(?P<lastName>[\p{Lu},\',\s,\-]+)'
            '\s?'
            '(\((?P<usageName>[\p{Lu},\',\s,\-]*)\))?'
            ), regex.UNICODE)

    name2Re = regex.compile(('(?P<firstName>([\p{L},\-,\']+\s){1,4})'
            '(?P<lastName>[\p{L},\',\s,\-]+)'
            '\s?'
            '(\((?P<usageName>[\p{Lu},\',\s,\-]*)\))?'
            ), regex.UNICODE)

    schools = {'11': 'Agro ParisTech',
            '15': 'Arts et Métiers ParisTech',
            '7': 'Chimie ParisTech',
            '10': 'Ponts ParisTech',
            '5': 'ENSAE ParisTech',
            '3': 'ENSTA ParisTech',
            '14': 'ESPCI ParisTech',
            '9': 'Mines ParisTech',
            '16': 'Institut Optique',
            '8' : 'Télécom ParisTech'}

    diplomes = {}

    rHeaders = {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }

    def parse(self, response):
        # First, we need to get all the keys and values for the
        # 100+ diplomas in the form
        diplValues = response.xpath('//select[@name="diplome"]//option//@value').extract()

        for val in diplValues:
            # if val == '0':
            #     continue

            text = response.xpath('//select[@name="diplome"]/option[@value="' + val + '"]/text()').extract()
            self.diplomes[str(val)] = text

        # Now, time for the POST requests
        params = 'nom_recherche=&prenom_recherche='

        for ecole in self.schools:
        # for ecole in self.schools.keys():
            params += '&ecole=' + str(ecole)

            for year in range(1876, 2016):
                params += '&promo=' + str(year)

                for diplome in self.diplomes.keys():
                    params += '&diplome=' + diplome
                    params += '&rech=1'
                    
                    request = scrapy.Request('http://www.paristech-alumni.org/gene/ajax_annuaireResults.php',
                            body = params,
                            method = 'POST',
                            headers = self.rHeaders,
                            callback = self.parseAlumnis)

                    request.meta['year'] = str(year)
                    request.meta['ecole'] = str(ecole)
                    request.meta['diplome'] = str(diplome)

                    yield request

    def parseAlumnis(self, response):
        "Parse results if any (making subrequests if necessary)"

        # Since only the first 200 results are displayed, with no
        # possibility to 'see more'. So we have to make subrequests
        # if we get that message.
        more = response.css('.confirmation').re('seuls')

        if more:
            letters = list(string.ascii_lowercase)

            # Subrequests with firstname containing each letter
            # of the alphabet
            for letter in letters:
                # if response.meta['prenom']:
                #     params = 'nom_recherche=' + letter + '&ae_nom_start=1'
                #     params += '&prenom_recherche=' + request.meta['prenom'] + '&ae_prenom_start=1'
                # else:
                params = 'nom_recherche='
                params += '&prenom_recherche=' + letter + '&ae_prenom_start=1'

                params += '&ecole=' + str(response.meta['ecole'])
                params += '&promo=' + response.meta['year']
                params += '&diplome=' + str(response.meta['diplome'])
                params += '&rech=1'

                request = scrapy.Request('http://www.paristech-alumni.org/gene/ajax_annuaireResults.php',
                        body = params,
                        method = 'POST',
                        headers = self.rHeaders,
                        callback = self.parseAlumnis)

                request.meta['year'] = response.meta['year']
                request.meta['ecole'] = response.meta['ecole']
                request.meta['diplome'] = response.meta['diplome']

                yield request

        # Let's grab all results (200 alumnis max) first 
        alumnis = response.xpath('//td//div//strong//text()').extract()
        # from scrapy.shell import inspect_response
        # inspect_response(response, self)

        for alumni in alumnis:
            name = alumni.strip()
            if name == '':
                continue

            match = self.nameRe.search(name)

            # If only one letter in lastName, switch to other pattern
            if not match:
                match = self.name2Re.search(name)

            usageName = ''
            if match.group('usageName'):
                usageName = match.group('usageName')

            load = ItemLoader(item=Alumni())

            load.add_value('lastName', match.group('lastName'))
            load.add_value('firstName', match.group('firstName'))
            load.add_value('usageName', usageName)
            load.add_value('year', response.meta['year'])
            load.add_value('school', self.schools[str(response.meta['ecole'])])
            load.add_value('status', self.diplomes[str(response.meta['diplome'])])

            yield load.load_item()

