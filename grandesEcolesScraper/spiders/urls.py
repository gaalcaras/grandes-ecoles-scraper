#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import os

def getTriplets():
    'Return all unique triplets from text file'

    filename = os.path.join(os.path.dirname(__file__), './unique-letters-triplet-fName.fr.txt')
    tripletsFile = open(filename, 'r')
    triplets = [line.strip() for line in tripletsFile.readlines()]

    return triplets

def tripletsUrl(beginUrl = '', endUrl = ''):
    'Generate StartUrls with triplets for spiders'

    triplets = getTriplets()
    startUrls = [beginUrl + triplet + endUrl for triplet in triplets]

    return startUrls

def rangeUrl(rangeMax = 2, beginUrl = '', endUrl = ''):
    'Generate StartUrls with range numbers for spiders'

    numbers = range(1, rangeMax)
    startUrls = [beginUrl + str(number) + endUrl for number in numbers]

    return startUrls
