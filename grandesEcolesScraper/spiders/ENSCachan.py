#!/usr/bin/python
# -*- coding: utf-8 -*-

import scrapy
import regex

from scrapy.loader import ItemLoader
from grandesEcolesScraper.items import Alumni 
import urls 

class ENSCachanAlumniSpider(scrapy.Spider):
    name = 'ENSCachan'
    start_urls = ['http://www.aae.ens-cachan.fr/annu-online.php']

    re = regex.compile(('^(?P<lastName>[\w,\',\-,\s]*\w)'
        '\s'
        '(\(\w{3}\s(?P<usageName>[\w,\-,\s,\']*\w*)\)\s)?'
        '(?P<firstName>[\w,\-]*)?'
        '(\s\(\s[\w,\(,\)]*\s\))?$'
        ), regex.UNICODE)

    def parse(self, response):
        form = {'TypeRech':'Promo'}

        request = scrapy.FormRequest.from_response(
                response,
                formdata = form,
                callback = self.promo_parse
                )

        request.meta['formdata'] = form

        return request

    def promo_parse(self, response):
        promos = response.xpath('//select[@name="promo"]//option//text()').re(r'(\d{4})')

        for promo in promos:
            form = {'promo': promo}
            formdata = response.meta['formdata']
            formdata.update(form)

            request = scrapy.FormRequest.from_response(
                    response,
                    formdata = form,
                    callback = self.section_parse
                    )

            request.meta['formdata'] = formdata

            yield request

    def section_parse(self, response):
        sections = response.xpath('//select[@name="section"]//option//@value').re(r'\d{2}')
        
        for section in sections:
            form = {'section': section}
            formdata = response.meta['formdata']
            formdata.update(form)

            request = scrapy.FormRequest.from_response(
                    response,
                    formdata = form,
                    callback = self.alumni_parse
                    )

            request.meta['formdata'] = formdata
            request.meta['section2_parsed'] = False

            yield request

    def alumni_parse(self, response):
        section2Parsed = response.meta['section2_parsed']

        if section2Parsed == False:
            sections2 = response.xpath('//select[@name="section2"]//option//@value').re(r'(\d\w)')

            if len(sections2) != 0:
                for section2 in sections2:
                    form = {'section2': section2}
                    formdata = response.meta['formdata']
                    formdata.update(form)

                    request = scrapy.FormRequest.from_response(
                            response,
                            formdata = form,
                            callback = self.alumni_parse
                            )

                    request.meta['formdata'] = formdata
                    request.meta['section2_parsed'] = True

                    yield request

        data = response.xpath('//option[@selected]//text()').extract()
        year = data[0]
        field = data[1]

        value = response.xpath('//option[@selected]//@value').extract()
        
        if len(value) == 2:
            schools = {'1K' : 'ENS Ker Lann',
                    '3K' : 'ENS Ker Lann',
                    '1C' : 'ENS Cachan',
                    '3C' : 'ENS Cachan'}
            statusList = {'1K' : 'Concours 1',
                    '3K' : 'Concours 3',
                    '1C' : 'Concours 1',
                    '3C' : 'Concours 3'}

            status = statusList[value[1]]
            school = schools[value[1]]
        else:
            status = ''
            school = 'ENS Cachan'

        alumnis = response.xpath('//p[@align]/following::p[1]/text()').re('.*\w.*')

        for alumni in alumnis:
            match = self.re.search(alumni.strip())

            load = ItemLoader(item=Alumni())

            load.add_value('lastName', match.group('lastName'))
            load.add_value('firstName', match.group('firstName'))
            load.add_value('usageName', match.group('usageName'))
            load.add_value('year', year)
            load.add_value('status', status)
            load.add_value('school', school)
            load.add_value('field', field)

            yield load.load_item()
