#!/usr/bin/python
# -*- coding: utf-8 -*-

import scrapy
import regex

from scrapy.loader import ItemLoader
from grandesEcolesScraper.items import Alumni 
import urls 

class XAlumniSpider(scrapy.Spider):
    name = 'X'
    start_urls = ['http://bibli-aleph.polytechnique.fr/F/?func=file&file_name=find-b&local_base=BCXC2']

    re = regex.compile(('^(?P<lastName>[\w,\',\-,\s,\u2019,/]*\w),'
        '\s'
        '(?P<firstName>[\w,\-]*)?.*'
        '\(X (?P<year>\d{4})$'
        ), regex.UNICODE)

    re2 = regex.compile(('^(?P<lastName>[\w,\',\-,\s,\u2019,/]*\w)'
        '\(X (?P<year>\d{4})$'
        ), regex.UNICODE)

    school = 'X'
    year = 1795
    yearMax = 2010

    def parse(self, response):
        form = {'request': str(self.year),
                'find_code': 'WYR',
                'adjacent': 'N'
                }
        request = scrapy.FormRequest.from_response(
                response,
                formdata = form,
                callback = self.alumniParse,
                )

        yield request
        
    def alumniParse(self, response):
        alumnis = response.xpath('//td[@colspan="2"]/text()').re('^[A-Z].*\(X \d{4}')

        # Now, check if there is more...
        nextPage = response.xpath('//a[@title="Next"]/@href').extract()

        if len(nextPage) > 0:
            yield scrapy.Request(nextPage[0],
                    callback = self.alumniParse)
        else:
            # If we are at the end of the current year list,
            # get the next one...

            # Unless we are at the end of the script.
            if self.year >= self.yearMax:
                return

            self.year += 1

            yield scrapy.Request(self.start_urls[0],
                    callback = self.parse,
                    dont_filter = True)

        for alumni in alumnis:
            match = self.re.search(alumni.strip())

            # Try to match against 2 possible patterns
            if match:
                lastName = match.group('lastName')
                firstName = match.group('firstName')
                year = match.group('year')
            else:
                match2 = self.re2.search(alumni.strip())

                lastName = match2.group('lastName')
                firstName = ''
                year = match2.group('year')

            # The current list doesn't have much information
            # about the alumnis, so we try a request on another database:
            urlX = 'https://www.polytechnique.org/search?quick='
            requestPattern = lastName + '+' + firstName + '+' + year

            request = scrapy.Request(urlX + requestPattern,
                    callback = self.usageNameParse)

            request.meta['lastName'] = lastName
            request.meta['firstName'] = firstName
            request.meta['year'] = year

            yield request


    def usageNameParse(self, response):
        reslastName = response.css('.popup2').re('>(.*) \(')
        status = response.css('div.edu').xpath('//img//@title').extract()
        usageName = ''

        if len(reslastName) != 0:
            usageName = response.meta['lastName']
            lastName = reslastName
        else:
            usageName = ''
            lastName = response.meta['lastName']

        load = ItemLoader(item=Alumni())

        load.add_value('lastName', lastName)
        load.add_value('usageName', usageName)
        load.add_value('firstName', response.meta['firstName'])
        load.add_value('year', response.meta['year'])

        load.add_value('status', status)
        load.add_value('school', self.school)

        yield load.load_item()
