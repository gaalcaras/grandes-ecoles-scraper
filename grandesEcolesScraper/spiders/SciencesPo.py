#!/usr/bin/python
# - coding: utf-8 -*-

import scrapy
import regex
import json

from scrapy.loader import ItemLoader
from grandesEcolesScraper.items import Alumni 
import urls 

class SciencesPoAlumniSpider(scrapy.Spider):
    name = 'SciencesPo'
    start_urls = ['http://www.sciences-po.asso.fr/gene/main.php?base=1244'] 

    nameRe = regex.compile(('^(?P<lastName>([\p{Lu},\',\s,\.,\-]*)?([\p{L},\',\s,\-,\d]*)?)'
            '\s+'
            '(\(\p{L}*\s(?P<usageName>[\p{L},\',\s,\.,\-]*)\)\s+)?'
            '(?P<firstName>\p{Lu}[\w,\-]*)'
            ), regex.UNICODE)

    rHeaders = {
            # 'Host': 'www.ensae.org',
            # 'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:42.0) Gecko/20100101 Firefox/42.0',
            # 'Accept': 'application/json, text/javascript, */*; q=0.01',
            # 'Accept-Language': 'fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3',
            # 'Accept-Encoding': 'gzip, deflate',
            # 'DNT': '1',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            # 'X-Requested-With': 'XMLHttpRequest',
            # 'Pragma': 'no-cache',
            # 'Cache-Control': 'no-cache',
            # 'Referer': 'https://www.ensae.org/gene/main.php?base=21',
            # 'Content-Length': str(len(params)),
            # 'Connection': 'keep-alive'
            }

    def parse(self, response):
        paramBase = 'base=1244&id_rech_entre=&PersonneNom=&PersonnePrenom=&DiplomeDiplome=&DiplomePromo='

        for year in range(1905, 2016):
            request = scrapy.Request('http://www.sciences-po.asso.fr/module/annuaire/ajax/countResults',
                    body = paramBase + str(year),
                    method = 'POST',
                    headers = self.rHeaders,
                    callback = self.countResults)

            request.meta['year'] = str(year)

            yield request

    def countResults(self, response):
        result = json.loads(response.body)

        url = 'http://www.sciences-po.asso.fr/module/annuaire/ajax/showResults'

        nbResults = result['nbResults']
        times = nbResults // 10

        for time in range(times+1):
            params = 'uniqueId=' + result['uniqueId']
            params += '&sortOrder=ASC'
            params += '&start=' + str(time*10)
            params += '&useview=card'
            params += '&nbResults=10'

            request = scrapy.Request(url,
                    body = params,
                    method = 'POST',
                    headers = self.rHeaders,
                    callback = self.showResults)

            request.meta['year'] = response.meta['year']

            yield request

    def showResults(self, response):
        alumnis = response.css('.ppl-content').xpath('h3/text()').extract()

        for alumni in alumnis:
            name = alumni.strip()

            if name == '':
                continue

            match = self.nameRe.search(name)

            usageName = ''

            if match.group('usageName'):
                usageName = match.group('usageName')

            load = ItemLoader(item=Alumni())

            load.add_value('lastName', match.group('lastName'))
            load.add_value('firstName', match.group('firstName'))
            load.add_value('usageName', usageName)
            load.add_value('year', response.meta['year'])
            load.add_value('school', self.name)

            yield load.load_item()
