#!/usr/bin/python
# -*- coding: utf-8 -*-

import scrapy
import regex

from scrapy.loader import ItemLoader
from grandesEcolesScraper.items import Alumni 
import urls 

class ENSLyonAlumniSpider(scrapy.Spider):
    name = 'ENSLyon'
    start_urls = urls.rangeUrl(16748, 'http://www.lyon-normalesup.org/Annuaire/frame.php?login_id=')

    def parse(self, response):
        name = response.xpath('//h3/b/text()').extract()
        lastName = regex.search(u'\s([\p{Lu},\-,\s]*)', name[0])

        url = 'http://www.lyon-normalesup.org/Annuaire/frame.php?nom=' + lastName.group(1)

        yield scrapy.Request(url, callback=self.parse_alumni)

    def parse_alumni(self, response):
        select = response.xpath('//select//option') 

        pattern = regex.compile(u'(?P<lastName>[\p{Lu},\-,\s]*) (?P<firstName>[\p{Lu}][\w,\-]*) \((née )?(?P<usageName>[\p{Lu},\-,\s]*)? ?(?P<field>[L,S]) (?P<school>[A-Z]{2}) (?P<year>[0-9]{4})')
        
        for option in select:
            alumni = option.xpath('./text()').extract()

            matched = regex.search(pattern, alumni[0])

            if matched:
                load = ItemLoader(item=Alumni())

                lyonFields = {'L': 'Lettres', 'S': 'Sciences'}
                lyonSchool= {'LY': 'ENS Lyon',
                        'FT': 'ENS Fontenay',
                        'FC': 'ENS Fontenay',
                        'SH': 'ENS LSH',
                        'SC': 'ENS Saint-Cloud'}

                school = lyonSchool[matched.group('school')]
                field = lyonFields[matched.group('field')]

                load.add_value('lastName', matched.group('lastName'))
                load.add_value('firstName', matched.group('firstName'))
                load.add_value('usageName', matched.group('usageName'))
                load.add_value('year', matched.group('year'))
                load.add_value('school', school)
                load.add_value('field', field)
                load.add_value('status', 'eleve')

                yield load.load_item()

