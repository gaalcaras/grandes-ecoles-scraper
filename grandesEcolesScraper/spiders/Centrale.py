#!/usr/bin/python
# - coding: utf-8 -*-

import scrapy
import regex
import json
import string

from scrapy.loader import ItemLoader
from scrapy.selector import Selector
from grandesEcolesScraper.items import Alumni 
import urls 

class CentraleAlumniSpider(scrapy.Spider):
    name = 'Centrale'
    start_urls = ['http://association.centraliens.net/addressbook/fullsearch/index'] 

    nameRe = regex.compile(('^(?P<firstName>[\p{L},\',\-]*)'
            '\s+'
            '(?P<lastName>[\p{L},\-,\',\s]*)'
            '\s?'
            '(\((?P<usageName>[\p{L},\',\s,\-]*)\))?'
            ), regex.UNICODE)

    rHeaders = {
            # 'Host': 'association.centraliens.net',
            # 'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:42.0) Gecko/20100101 Firefox/42.0',
            # 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            # 'Accept-Language': 'fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3',
            # 'Accept-Encoding': 'gzip, deflate',
            # 'DNT': '1',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'X-Requested-With': 'XMLHttpRequest',
            # 'Pragma': 'no-cache',
            # 'Cache-Control': 'no-cache',
            # 'Referer': 'http://association.centraliens.net/addressbook/fullsearch/index',
            # 'Content-Length': str(len(params)),
            # 'Connection': 'keep-alive'
            }

    def parse(self, response):
        tag = response.xpath('//script').re('postform_(generated_tagid[\w,\d]{14}_[\w,\d]{8})')
        paramBase = 'firstname=b&lastname=b&undefined=&_plid=' + tag[0]
        alphabet = list(string.ascii_lowercase)
        url = 'http://association.centraliens.net/taglib/sectionupdate/update?updatelist=content:action://addressbook/public-search/search'

        for firstName in alphabet:
            params = 'firstname=' + firstName

            for lastName in alphabet:
                params += '&lastname=' + lastName
                params += '&undefined=&_plid=' + tag[0]

                request = scrapy.Request(url,
                        body = params,
                        method = 'POST',
                        headers = self.rHeaders,
                        callback = self.countResults)

                yield request

    def countResults(self, response):
        result = json.loads(response.body)
        body = Selector(text=result['content'])
        lis = body.xpath('//li')

        for li in lis:
            em = li.xpath('(.//em)/text()').extract()
            name = em[0].strip()

            if name == '':
                continue

            dipl = li.xpath('(./text())').re('validation')

            if dipl:
                continue

            match = self.nameRe.search(name)

            usageName = ''

            if match.group('usageName'):
                usageName = match.group('usageName')

            load = ItemLoader(item=Alumni())

            load.add_value('lastName', match.group('lastName'))
            load.add_value('firstName', match.group('firstName'))
            load.add_value('usageName', usageName)
            load.add_value('year', '0000')
            load.add_value('school', self.name)

            yield load.load_item()
