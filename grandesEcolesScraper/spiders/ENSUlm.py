#!/usr/bin/python
# -*- coding: utf-8 -*-

import scrapy
import re
from scrapy.loader import ItemLoader
from grandesEcolesScraper.items import Alumni 
import urls 

class AUlmAlumniSpider(scrapy.Spider):
    name = 'AUlm'
    start_urls = urls.tripletsUrl('http://www.archicubes.ens.fr/annuaire/annuaire_chercher?identite=')

    def parse(self, response):
        rows = response.xpath('//tr[td]')
        
        for row in rows:
            load = ItemLoader(item=Alumni(), selector=row)
            load.add_xpath('lastName', '(.//td)[1]')
            load.add_xpath('firstName', '(.//td)[2]')
            load.add_xpath('usageName', '(.//td)[3]')

            # The last column contains lots of additional data
            lastColumn = row.xpath('(.//td)[4]/text()').extract()
            lastColumn = str(lastColumn)

            ulmFields = {'l': 'Lettres', 's': 'Sciences'}

            yearMatched = re.search(r'(\d{4})', lastColumn)
            sevresMatched = re.search(r'\d{4} ([L,S])', lastColumn)
            ulmMatched = re.search(r'\d{4} ([l,s])', lastColumn)
            diplMatched = re.search(r'dipl\.', lastColumn)
            PEMatched = re.search(r'P\.E\.', lastColumn)

            status = 'eleve'
            school = 'Ulm'

            # Sometimes, year is not specified
            if yearMatched:
                year = yearMatched.group(1)
            else:
                year = ''

            if PEMatched:
            	status = "PE"

            # Trying to match the school (missing for: 
            # pensionnaires étrangers, diplômant·e·s, etc., so
            # defaulting to 'Ulm')
            if sevresMatched:
                school = 'Sèvres'
                field = ulmFields[sevresMatched.group(1).lower()]
            elif ulmMatched:
                field = ulmFields[ulmMatched.group(1)]
            else:
                field = ''
                if int(year)>=1810:
                    status = 'autre'

                    if diplMatched:
		        status = "diplome"
                    
                    

            load.add_value('year', year)
            load.add_value('school', school)
            load.add_value('field', field)
            load.add_value('status', status)

            yield load.load_item()

